﻿using System;
using ConsoleDungeon.HeroSetup;
using ConsoleDungeon.Mechanics;

namespace ConsoleDungeon
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero playerHero = new Hero();

            Greeting.Greeting.Welcoming(playerHero);

            Mechanics.TheFight.BeginFight(playerHero);

            Console.ReadLine();
        }
    }
}