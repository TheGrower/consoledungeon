﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleDungeon.HeroSetup;

namespace ConsoleDungeon.Greeting
{
    class Greeting
    {
        public static void Welcoming(Hero playerHero)
        {
            Console.WriteLine("Welcome to the dungeon. Be prepared for death.");
            Console.WriteLine("Before your eagerness sends you headlong into the depths, you must register.");
            Console.WriteLine("Let us start with a few basics, shall we? Press Enter when you are ready.");
            Console.ReadLine();

            playerHero.heroName = HeroSetup.BuildCharacter.Set_HeroName(playerHero, out playerHero.heroName);
            playerHero.heroRace = HeroSetup.BuildCharacter.Set_HeroRace(playerHero, out playerHero.heroRace);
            playerHero.heroClass = HeroSetup.BuildCharacter.Set_HeroClass(playerHero, out playerHero.heroClass);

            Hero.Get_RaceAttributes(playerHero);
            Hero.Get_ClassAttributes(playerHero);

            Console.WriteLine("Name: {0}\nRace: {1}\nClass: {2}", playerHero.heroName, playerHero.heroRace, playerHero.heroClass);
            Console.WriteLine("HP: {0}\nStr: {1}\nDex: {2}\nWis: {3}\nInt: {4}\nDef: {5}\nSpd: {6}\nArm: {7}\nDefX: {8}\nHit: {9}",
                playerHero.heroHitPoints, playerHero.heroStrength, playerHero.heroDexterity, playerHero.heroWisdom, playerHero.heroIntelligence,
                playerHero.heroDefense, playerHero.heroSpeed, playerHero.heroArmorRating, playerHero.heroDefenseMultiplier, playerHero.heroHitChance);
            Console.Read();
        }
    }
}
