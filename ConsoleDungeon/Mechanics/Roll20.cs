﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDungeon.Mechanics
{
    class Roll20
    {
        private static Random roll = new Random();

        public static int _Roll20()
        {
            int dice20 = roll.Next(1, 21);

            return dice20; 
        }
    }
}
