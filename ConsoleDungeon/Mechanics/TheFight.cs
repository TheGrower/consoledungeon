﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleDungeon.HeroSetup;
using ConsoleDungeon.Enemies;
using ConsoleDungeon.Mechanics;

namespace ConsoleDungeon.Mechanics
{
    class TheFight
    {
        Spider spider = new Spider();
        Hero playerHero = new Hero();

        public static void BeginFight (Hero playerHero)
        {
            Spider spider = new Spider();

            Console.Clear();
            Console.WriteLine("Hero HP: {0}", playerHero.heroHitPoints);
            Console.WriteLine("Spider HP: {0}\n\n\n", spider.spiderHitPoints);


            int enemyHit = 0;
            int heroHit = 0;

            int determineFightStarter = Roll20._Roll20();

            if (determineFightStarter % 2 == 0)
            {
                Console.WriteLine("You have encountered a common cave spider, prepare to attack");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("A common cave spider has surprised you when you lowered your guard.");
                Console.ReadLine();

                enemyHit = Convert.ToInt32(Math.Round(0.3 * Roll20._Roll20()));

                playerHero.heroHitPoints -= enemyHit;
                Console.Clear();
                Console.WriteLine("Hero HP: {0}", playerHero.heroHitPoints);
                Console.WriteLine("Spider HP: {0}\n\n\n", spider.spiderHitPoints);
                Console.WriteLine("The spider strikes you for {0} damage.", enemyHit);
                Console.ReadLine();
            }

            while (playerHero.heroHitPoints > 0 && spider.spiderHitPoints > 0)
            {
                heroHit = Convert.ToInt32(Math.Round(0.4 * Roll20._Roll20()));

                spider.spiderHitPoints -= spider.spiderHitPoints - heroHit <= 0
                    ? spider.spiderHitPoints
                    : heroHit;

                Console.Clear();
                Console.WriteLine("Hero HP: {0}", playerHero.heroHitPoints);
                Console.WriteLine("Spider HP: {0}\n\n\n", spider.spiderHitPoints);
                Console.WriteLine("You hit the spider for {0} damage.", heroHit);
                Console.ReadLine();


                if(spider.spiderHitPoints > 0)
                {
                    enemyHit = Convert.ToInt32(Math.Round(0.3 * Roll20._Roll20()));

                    playerHero.heroHitPoints -= playerHero.heroHitPoints - enemyHit <= 0
                        ? playerHero.heroHitPoints
                        : enemyHit;

                    Console.Clear();
                    Console.WriteLine("Hero HP: {0}", playerHero.heroHitPoints);
                    Console.WriteLine("Spider HP: {0}\n\n\n", spider.spiderHitPoints);

                    Console.WriteLine("The spider strikes you for {0} damage.", enemyHit);
                    Console.ReadLine();
                }

                if (playerHero.heroHitPoints <= 0)
                {
                    Console.WriteLine("You Died.");
                    Console.ReadLine();
                }
                else if(spider.spiderHitPoints <=0)
                {
                    Console.WriteLine("You have slain the spider.");
                    Console.ReadLine();
                }
                else
                {
                    continue;
                }
            }
        }

    }
}
