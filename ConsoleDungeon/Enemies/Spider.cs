﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDungeon.Enemies
{
    class Spider : BaseEnemy
    {
        public string spiderName = "Common Spider";

        public int spiderHitPoints = 30;
        public int spiderStrength = 5;
        public int spiderDexterity = 5;
        public int spiderWisdom = 0;
        public int spiderIntelligence = 3;
        public int spiderDefense = 2;
        public int spiderSpeed = 5;
        public int spiderArmorRating = 3;
        public int spiderDefenseMultiplier = 1;
        public int spiderHitChance = 3;
        public int spiderDamage = 3;
    }
}
