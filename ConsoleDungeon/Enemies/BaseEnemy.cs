﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDungeon.Enemies
{
    class BaseEnemy
    {
        public string enemyName = "";

        public int enemyHitPoints = 0;
        public int enemyStrength = 0;
        public int enemyDexterity = 0;
        public int enemyWisdom = 0;
        public int enemyIntelligence = 0;
        public int enemyDefense = 0;
        public int enemySpeed = 0;
        public int enemyArmorRating = 0;
        public int enemyDefenseMultiplier = 0;
        public int enemyHitChance = 0;
        public int enemyDamage = 0;

        private static void Set_EnemyAttributes(BaseEnemy baseEnemy, string name, int hitPoints, int strength, int dexterity, int wisdom, int intelligence,
            int defense, int speed, int armorRating, int defenseMultiplier, int hitChance, int damage)
        {
            baseEnemy.enemyName = name;
            baseEnemy.enemyHitPoints = hitPoints;
            baseEnemy.enemyStrength = strength;
            baseEnemy.enemyDexterity = dexterity;
            baseEnemy.enemyWisdom = wisdom;
            baseEnemy.enemyIntelligence = intelligence;
            baseEnemy.enemyDefense = defense;
            baseEnemy.enemySpeed = speed;
            baseEnemy.enemyArmorRating = armorRating;
            baseEnemy.enemyDefenseMultiplier = defenseMultiplier;
            baseEnemy.enemyHitChance = hitChance;
            baseEnemy.enemyDamage = damage;
        }
    }
}
