﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDungeon.HeroSetup
{
    class BuildCharacter
    {
        public static string Set_HeroName(Hero chosenHero, out string chosenName)
        {
            Console.WriteLine("What is your name, hero?");
            chosenName = Console.ReadLine();

            return chosenName;
        }

        public static HeroRace Set_HeroRace(Hero chosenHero, out HeroRace chosenRace)
        {
            int choice = 0;
            chosenRace = HeroRace.Human; //assigned to human as default. 
            
            Console.WriteLine();
            Console.WriteLine("What is your Hero's Race:  ");
            Console.WriteLine("Option 1 -- Human");
            Console.WriteLine("Option 2 -- Elf");
            Console.WriteLine("Option 3 -- Dwarf");
            Console.WriteLine("Option 4 -- Keplarian");
            
            choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    chosenRace = HeroRace.Human;
                    break;
                case 2:
                    chosenRace = HeroRace.Elf;
                    break;
                case 3:
                    chosenRace = HeroRace.Dwarf;
                    break;
                case 4:
                    chosenRace = HeroRace.Keplarian;
                    break;
                default:
                    Console.Write("Dueces!");
                    break;
            }

            return chosenRace;
        }

        public static HeroClass Set_HeroClass(Hero chosenHero, out HeroClass chosenClass)
        {
            int choice = 0;
            chosenClass = HeroClass.Shield_Warrior;  //assigned to Shield Warrior as default. 

            Console.WriteLine();
            Console.WriteLine("What is your Hero's Class:  ");
            Console.WriteLine("Option 1 -- Fire Mage");
            Console.WriteLine("Option 2 -- Frost Mage");
            Console.WriteLine("Option 3 -- Shield Warrior");
            Console.WriteLine("Option 4 -- Hammer Warrior");
            Console.WriteLine("Option 5 -- Dagger Rogue");
            Console.WriteLine("Option 6 -- Ranged Rogue");

            choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    chosenClass = HeroClass.Fire_Mage;
                    break;
                case 2:
                    chosenClass = HeroClass.Frost_Mage;
                    break;
                case 3:
                    chosenClass = HeroClass.Shield_Warrior;
                    break;
                case 4:
                    chosenClass = HeroClass.Hammer_Warrior;
                    break;
                case 5:
                    chosenClass = HeroClass.Dagger_Rogue;
                    break;
                case 6:
                    chosenClass = HeroClass.Ranged_Rogue;
                    break;
                default:
                    Console.Write("Dueces!");
                    break;
            }

            return chosenClass;
        }
    }
}
