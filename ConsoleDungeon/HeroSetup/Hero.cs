﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDungeon.HeroSetup
{
    public enum HeroClass
    {
        Fire_Mage,
        Frost_Mage,
        Shield_Warrior,
        Hammer_Warrior,
        Dagger_Rogue,
        Ranged_Rogue
    };

    public enum HeroRace
    {
        Human,
        Elf,
        Dwarf,
        Keplarian
    };

    class Hero
    {
        public string heroName = "";
        public HeroRace heroRace = new HeroRace();
        public HeroClass heroClass = new HeroClass();

        public int heroLevel = 0;
        public int heroHitPoints = 0;
        public int heroStrength = 0;
        public int heroDexterity = 0;
        public int heroWisdom = 0;
        public int heroIntelligence = 0;
        public int heroDefense = 0;
        public int heroSpeed = 0;
        public int heroArmorRating = 0;
        public int heroDefenseMultiplier = 0;
        public int heroHitChance = 0;
        public int heroDamage = 0;

        public void Get_NameRaceClass(string name, HeroRace raceChoice, HeroClass classChoice)
        {
            heroName = name;
            heroRace = raceChoice;
            heroClass = classChoice;
        }

        private static void Set_RaceAttributes(Hero chosenHero, int hitPoints, int strength, int dexterity, int wisdom, int intellect, int defense, int speed)
        {
            chosenHero.heroHitPoints = hitPoints;
            chosenHero.heroStrength = strength;
            chosenHero.heroDexterity = dexterity;
            chosenHero.heroWisdom = wisdom;
            chosenHero.heroIntelligence = intellect;
            chosenHero.heroDefense = defense;
            chosenHero.heroSpeed = speed;
        }

        public static void Get_RaceAttributes(Hero chosenHero)
        {
            switch (chosenHero.heroRace)
            {
                case HeroRace.Human:
                    Set_RaceAttributes(chosenHero, 10, 1, 0, 0, 0, 1, 0);
                    break;
                case HeroRace.Elf:
                    Set_RaceAttributes(chosenHero, 0, 0, 0, 1, 1, 0, 1);
                    break;
                case HeroRace.Dwarf:
                    Set_RaceAttributes(chosenHero, 10, 1, 0, 0, 0, 1, 0);
                    break;
                case HeroRace.Keplarian:
                    Set_RaceAttributes(chosenHero, 0, 0, 2, -2, 3, -2, 2);
                    break;
                default:
                    Set_RaceAttributes(chosenHero, 0, 0, 0, 0, 0, 0, 0);
                    break;
            }
        }

        private static void Set_ClassAttirbutes(Hero chosenHero, int raceHitPoints, int raceStrength, int raceDexerity, int raceWisdom, int raceIntelligence, int raceDefense, int raceSpeed,
            int armorRating, int defenseMultiplier, int hitChance, int raceDamage)
        {
            chosenHero.heroHitPoints += raceHitPoints;
            chosenHero.heroStrength += raceStrength;
            chosenHero.heroDexterity += raceDexerity;
            chosenHero.heroWisdom += (chosenHero.heroWisdom + raceWisdom < 0)
                   ? 0
                   : raceWisdom;
            chosenHero.heroIntelligence += raceIntelligence;
            chosenHero.heroDefense += raceDefense;
            chosenHero.heroSpeed += raceSpeed;
            chosenHero.heroArmorRating += armorRating;
            chosenHero.heroDefenseMultiplier += defenseMultiplier;
            chosenHero.heroHitChance += hitChance;
            chosenHero.heroDamage += raceDamage;
        }

        public static void Get_ClassAttributes(Hero chosenHero)
        {
            switch (chosenHero.heroClass)
            {
                case HeroClass.Fire_Mage:
                    Set_ClassAttirbutes(chosenHero, 60, 2, 5, 7, 10, 2, 5, 15, 3, 4, 5);
                    break;
                case HeroClass.Frost_Mage:
                    Set_ClassAttirbutes(chosenHero, 60, 2, 5, 7, 10, 3, 5, 20, 3, 4, 3);
                    break;
                case HeroClass.Shield_Warrior:
                    Set_ClassAttirbutes(chosenHero, 100, 10, 4, 0, 5, 8, 4, 55, 5, 2, 8);
                    break;
                case HeroClass.Hammer_Warrior:
                    Set_ClassAttirbutes(chosenHero, 100, 10, 6, 0, 4, 7, 4, 55, 5, 2, 10);
                    break;
                case HeroClass.Dagger_Rogue:
                    Set_ClassAttirbutes(chosenHero, 70, 5, 9, 0, 5, 4, 8, 30, 7, 3, 5);
                    break;
                case HeroClass.Ranged_Rogue:
                    Set_ClassAttirbutes(chosenHero, 70, 4, 10, 0, 7, 3, 7, 35, 6, 5, 5);
                    break;
                default:
                    break;
            }
        }
    }
}
